Containerisation and Orchestration - exercise 04
================================================

[[_TOC_]]

Using K9s
---------

Seems to be a popular terminal client.  Install with (https://k9scli.io/topics/install/):
```
$ wget -O - https://github.com/derailed/k9s/releases/download/v0.24.13/k9s_Linux_x86_64.tar.gz | tar -xzvf - k9s && sudo mv k9s /usr/local/bin/k9s
```
For  macos use:
```
$ brew install derailed/k9s/k9s
```
For Windows:
```
choco install k9s
```

The guide for how to use `k9s` is here https://k9scli.io/ .

Kubernetes API
--------------
From https://kubernetes.io/docs/tasks/debug-application-cluster/resource-metrics-pipeline/

The Kubernetes API follows the ReST based methodology, and is OpenAPI v2 compliant.  This means that it is relatively easy to 'poke and prod' the API with a command line tool like `curl`.  To do this, we setup an authenticated proxy using `kubectl`:
```
$ kubectl proxy --port=8080
Starting to serve on 127.0.0.1:8080
```

And then you can hit the API.  Try the following to access the metrics interface for example:
```console
$ curl http://localhost:8080/apis/metrics.k8s.io/v1beta1/pods
```

And this to get the details of the NGINX Ingress Controller `Deployment` resource deployed on Minikube:
```console
$ curl -X 'GET' \
  'http://localhost:8080/apis/apps/v1/namespaces/ingress-nginx/deployments/ingress-nginx-controller?pretty=true' \
  -H 'accept: application/json'
```

We can also view the API definitions using the [Swagger UI](https://swagger.io/tools/swagger-ui/) for [OpenAPI v2](https://swagger.io/specification/) - try the following to export the interface definitions and then launch the Swagger UI viewer:
```console
$ curl localhost:8080/openapi/v2 > /tmp/temp.json
$ docker run --rm -it -p 9999:8080 \
    -e SWAGGER_JSON=/var/specs/temp.json \
    -v /tmp/temp.json:/var/specs/temp.json \
    swaggerapi/swagger-ui
```

Now point your browser at: http://localhost:9999/ to open the Swagger UI, for inspecting the OpenAPI V2 specs.


Introduction to Kubernetes Primitives
-------------------------------------

This directory contains a list of examples that show how the basic Kubernetes primitives are used to deploy an application.  Examples can be found in  the `./ex04` directory.

These examples are best explored using [Minikube](https://minikube.sigs.k8s.io/docs/start/), with an [Ingress Controller](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) deployed.  The easiest way to do this (for Linux and MacOS users) is to use https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube , which will ensure that Minikube is deployed with the necessary features and the correct commandline tools are in place (such as `helm` and `kubectl`).

Ensure that the Ingress Controller is activated:
```console
$ minikube addons list
|-----------------------------|----------|--------------|
| ADDON NAME                    | PROFILE    | STATUS         |
| ----------------------------- | ---------- | -------------- |
| ambassador                    | minikube   | disabled       |
| auto-pause                    | minikube   | disabled       |
| csi-hostpath-driver           | minikube   | disabled       |
| dashboard                     | minikube   | disabled       |
| default-storageclass          | minikube   | enabled ✅      |
| efk                           | minikube   | disabled       |
| freshpod                      | minikube   | disabled       |
| gcp-auth                      | minikube   | disabled       |
| gvisor                        | minikube   | disabled       |
| helm-tiller                   | minikube   | disabled       |
| ingress                       | minikube   | disabled       |
| ingress-dns                   | minikube   | disabled       |
| istio                         | minikube   | disabled       |
| istio-provisioner             | minikube   | disabled       |
| kubevirt                      | minikube   | disabled       |
| logviewer                     | minikube   | disabled       |
| metallb                       | minikube   | disabled       |
| metrics-server                | minikube   | disabled       |
| nvidia-driver-installer       | minikube   | disabled       |
| nvidia-gpu-device-plugin      | minikube   | disabled       |
| olm                           | minikube   | disabled       |
| pod-security-policy           | minikube   | disabled       |
| registry                      | minikube   | disabled       |
| registry-aliases              | minikube   | disabled       |
| registry-creds                | minikube   | disabled       |
| storage-provisioner           | minikube   | enabled ✅      |
| storage-provisioner-gluster   | minikube   | disabled       |
| volumesnapshots               | minikube   | disabled       |
| ----------------------------- | ---------- | -------------- |
$ minikube addons enable ingress
    ▪ Using image k8s.gcr.io/ingress-nginx/controller:v0.44.0
    ▪ Using image docker.io/jettech/kube-webhook-certgen:v1.5.1
    ▪ Using image docker.io/jettech/kube-webhook-certgen:v1.5.1
🔎  Verifying ingress addon...
🌟  The 'ingress' addon is enabled
```

The objective of these examples are to walk through creating a scalable application deployment that consists of a `Deployment`, load-balanced with a `Service`, and exposed to the end user with an `Ingress`.

Launching a Deployment
----------------------

For this set of examples, we will use the `echoserver` container [first referenced in](./ex01).

First, we will launch a basic Pod for the `echoserver`.  Actually, running bare Pods is discouraged as they do not have any of the advantages of healthcheck monitoring, and rolling updates that Deployments etc. have.

Start the `echoserver`:
```console
$ kubectl run echoserver --image=gcr.io/google_containers/echoserver:1.4
```

Watch to see when it has reached `Running` state:
```console
$ kubectl get pods --watch
NAME         READY   STATUS    RESTARTS   AGE
echoserver   1/1     Running   0          27s
# Note: ctrl-c to exit
```

Describe the details of the running Pod with:
```console
$ kubectl describe pod/echoserver
Name:         echoserver
Namespace:    default
Priority:     0
Node:         minikube/192.168.49.2
Start Time:   Wed, 23 Jun 2021 02:26:19 +1200
Labels:       run=echoserver
Annotations:  <none>
Status:       Running
IP:           172.17.0.3
IPs:
  IP:  172.17.0.3
Containers:
  echoserver:
    Container ID:   docker://4c77e02b837fdc81e34c64eb3a1bcefc917e64be2d7a3bee9bad972bdbeaf1b1
    Image:          gcr.io/google_containers/echoserver:1.4
    Image ID:       docker-pullable://gcr.io/google_containers/echoserver@sha256:5d99aa1120524c801bc8c1a7077e8f5ec122ba16b6dda1a5d3826057f67b9bcb
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Wed, 23 Jun 2021 02:26:20 +1200
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-t2629 (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  kube-api-access-t2629:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  10m   default-scheduler  Successfully assigned default/echoserver to minikube
  Normal  Pulled     10m   kubelet            Container image "gcr.io/google_containers/echoserver:1.4" already present on machine
  Normal  Created    10m   kubelet            Created container echoserver
  Normal  Started    10m   kubelet            Started container echoserver
```

Now setup a `port-forward` to the running Pod in the Kubernetes cluster so that you can access the echoserver locally:
```console
$ kubectl port-forward pod/echoserver 8080
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
# Note: ctrl-c to exit
```

Test the `echoserver` using `curl` in a different terminal (or your browser), by going to:
```console
$ curl http://127.0.0.1:8080
CLIENT VALUES:
client_address=127.0.0.1
command=GET
real path=/
query=nil
request_version=1.1
request_uri=http://127.0.0.1:8080/

SERVER VALUES:
server_version=nginx: 1.10.0 - lua: 10001

HEADERS RECEIVED:
accept=*/*
host=127.0.0.1:8080
user-agent=curl/7.68.0
BODY:
-no body in request-
```

Now lets tidy up:
```console
$ kubectl delete pod/echoserver
pod "echoserver" deleted
```

Launching the same application in a `Deployment` enables the specification of additional properties such as the replication factor.

Save the following into a file `ex04-echo-deployment.yaml`:
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: echoserver
  labels:
    app: echoserver
spec:
  replicas: 3
  selector:
    matchLabels:
      app: echoserver
  template:
    metadata:
      labels:
        app: echoserver
    spec:
      containers:
      - image: gcr.io/google_containers/echoserver:1.4
        imagePullPolicy: Always
        name: echoserver
        ports:
        - containerPort: 8080
```

Now, deploy it with `kubectl`:
```console
$ kubectl apply -f ex04-echo-deployment.yaml
deployment.apps/echoserver created
```

Check that all 3 replicas are up and running:
```console
$ kubectl get deployment --watch
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
echoserver   3/3     3            3           41s
# Note: ctrl-c to exit
$ kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
echoserver-7dd65ffdd6-6xjhh   1/1     Running   0          93s
echoserver-7dd65ffdd6-f5rqf   1/1     Running   0          93s
echoserver-7dd65ffdd6-kk75t   1/1     Running   0          93s
```
This can then be cleaned up with:
```console
$ kubectl delete -f ex04-echo-deployment.yaml
deployment.apps "echoserver" deleted
```

Note: because of the eventual consistency nature of control plane management of resources, the Pods will not be deleted instantly or synchronously with the request.


Exposing the Deployment with a Service
--------------------------------------

The `Service` resource is a networking component that exposes and load balances Ports from Pods both internally and externally to the Kubernetes cluster.  `Service`s come in three main flavours - `ClusterIP` , `NodePort`, and `LoadBalancer`.
* `ClusterIP` places the loadbalanced Pods behind a single IP address with an internally resolvable DNS name that is accessible from any node in the Kubernetes cluster (subject to network policy rules).
* `NodePort` makes the exposed Ports from the associated Pods accessible from a defined Port on the *host* network of the Nodes in the Kubernetes cluster.
* `LoadBalancer` is used for 3rd party integration to expose a `Service` via an external mechanism usually provided by a Cloud Provider, such as Octavia (OpenStack), ELB (AWS), etc.
* `ExternalName` is another type that is used to map a `Service` to a DNS name.  This can be used to map to external services, or maintain alternative names.

In the following example we will expose the load balanced Pods of the `echoserver` via a `ClusterIP` `Service`, which will then be addressable by DNS within the cluster.

Redeploy the `Deployment` from above, and check:
```console
$ kubectl apply -f ex04-echo-deployment.yaml
deployment.apps/echoserver created
```

Save the following in a file `ex04-echo-service.yaml`:
```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: echoserver
spec:
  ports:
    - port: 80
      targetPort: 8080
      protocol: TCP
  type: ClusterIP
  selector:
    app: echoserver
```

And deploy with:
```console
$ kubectl apply -f ex04-echo-service.yaml
service/echoserver created
```

Check that the `Service` is configured:
```console
$ kubectl get service --watch
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
echoserver   ClusterIP   10.102.62.190   <none>        80/TCP    93s
# Note: ctrl-c to exit
```

As before with the bare Pod, we can use the same techniques to check the `Service` - start a port-forward to the `Service` mapping Port 80 to the localhost 8080:
```console
$ kubectl port-forward svc/echoserver 8080:80
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
Handling connection for 8080
# Note: ctrl-c to exit
```

Test with `curl`:
```console
$ curl http://127.0.0.1:8080
CLIENT VALUES:
client_address=127.0.0.1
command=GET
real path=/
...
```

And tidy up with:
```console
$ kubectl delete -f ex04-echo-deployment.yaml
deployment.apps "echoserver" deleted
$ kubectl delete -f ex04-echo-service.yaml
service "echoserver" deleted
```

Exposing the Service to the Outside World
-----------------------------------------

A `Service` is exposed to the outside world using an `Ingress`.  An `Ingress`, is a Kubernetes abstraction of a network proxy, that does the necessary translation of external HTTP/HTTPS requests to the target `Service` that intern is a load-balancer to the underlying running application `Pod`s.  The network proxy is deployed as an Ingress Controller, for which there are a number of [available implementations](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/).  Minikube uses the [Nginx Ingress Controller](https://github.com/kubernetes/ingress-nginx), follow these instructions to enable it https://minikube.sigs.k8s.io/docs/tutorials/nginx_tcp_udp_ingress/#enable-the-ingress-addon.


Redeploy the `Deployment` and `Service` from above, and check:
```console
$ kubectl apply -f ex04-echo-deployment.yaml
deployment.apps/echoserver created
$ kubectl apply -f ex04-echo-service.yaml
service/echoserver created
```

Create an `Ingress` for the `Service` resource created above, so that we can expose the `echoserver` to traffice outside of the Kubernetes cluster.

Save the following in a file `ex04-echo-ingress.yaml`:
```yaml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: echoserver
  annotations:
    kubernetes.io/ingress.class: nginx
spec:
  rules:
    - http:
        paths:
          - path: /ping
            pathType: Prefix
            backend:
              service:
                name: echoserver
                port:
                  number: 80
```

And deploy with:
```console
$ kubectl apply -f ex04-echo-ingress.yaml
ingress.networking.k8s.io/echoserver created
```

Check that the `Ingress` is configured:
```console
$ kubectl get ingress --watch
NAME         CLASS    HOSTS   ADDRESS   PORTS   AGE
echoserver   <none>   *                 80      20s
# Note: ctrl-c to exit
```

Now we can test the `Ingress` access without the need for port forwarding or tunnelling into the cluster.

Test with `curl`:
```console
$ curl http://$(minikube ip)/ping
CLIENT VALUES:
client_address=172.17.0.2
command=GET
real path=/ping
query=nil
request_version=1.1
request_uri=http://192.168.49.2:8080/ping

SERVER VALUES:
server_version=nginx: 1.10.0 - lua: 10001

HEADERS RECEIVED:
accept=*/*
host=192.168.49.2
user-agent=curl/7.68.0
x-forwarded-for=192.168.49.3
x-forwarded-host=192.168.49.2
x-forwarded-port=80
x-forwarded-proto=http
x-real-ip=192.168.49.1
x-request-id=3994a8c975be3e466c3988dff6c0a89d
x-scheme=http
BODY:
-no body in request-
```

And tidy up with:
```console
$ kubectl delete -f ex04-echo-deployment.yaml
deployment.apps "echoserver" deleted
$ kubectl delete -f ex04-echo-service.yaml
service "echoserver" deleted
$ kubectl delete -f ex04-echo-ingress.yaml
ingress.networking.k8s.io "echoserver" deleted
```

Roundup
-------

We have now been through the complete process of deploying an application (`Deployment`), exposing and load-balancing the deployed application replicas with a `Service`, and finally exposing the application all the way outside of the Kubernetes cluster with an `Ingress`.

[Home &#x3e;&#x3e;](../README.md)
