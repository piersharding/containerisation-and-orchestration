#!/bin/sh

NAME=$1
if [ -z "${NAME}" ]; then
    echo "Must supply container NAME as argument."
    exit 1
fi

printf "Cleaning up ${NAME}\n"
docker inspect ${NAME} >/dev/null 2>&1 && docker rm -f ${NAME}
