#!/bin/bash

NAME=ex01-echoserver
CMD="docker run --rm --name ${NAME} -p 8080:8080 -d gcr.io/google_containers/echoserver:1.4"
URL="http://0.0.0.0:8080"
BASEDIR=$(dirname "$0")

# check that the container is not running
${BASEDIR}/clean.sh ${NAME}

# Launch container with sleep 1 second to allow to settle
printf "Launching echoserver with: ${CMD} \n"
RES=$(${CMD})
sleep 1

# print instructions and then open echoserver in  browser
printf "test the echoserver with: curl http://0.0.0.0:8080\nWhen finished, cleanup with: docker rm -f ${NAME}\n or use the script '${BASEDIR}/clean.sh ${NAME}' \n\n"
if [[ ${HOME} == *"Users"* ]]; then
    open ${URL}
else
    xdg-open ${URL}
fi
printf "Finished.\n"
exit 0
