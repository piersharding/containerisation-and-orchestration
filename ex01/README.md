Containerisation and Orchestration - exercise 01
================================================

[[_TOC_]]

Running containers with Docker
------------------------------

This directory contains an example of running a basic `echoserver` in a container using Docker.  It also walks through using some common Docker commands to inspect and manage containers and images.

Launch a container:
```console
$ docker run --rm --name ex01-echoserver -p 8080:8080 -d gcr.io/google_containers/echoserver:1.4
Unable to find image 'gcr.io/google_containers/echoserver:1.4' locally
1.4: Pulling from google_containers/echoserver
6d9e6e7d968b: Pulling fs layer
a3ed95caeb02: Pulling fs layer
cd23f57692f8: Pulling fs layer
412c0feed608: Pulling fs layer
dcd34d50d5ee: Pulling fs layer
d3c51dabc842: Pulling fs layer
b4241160ce0e: Pulling fs layer
7abee76f69c0: Pulling fs layer
d3c51dabc842: Waiting
412c0feed608: Waiting
7abee76f69c0: Waiting
b4241160ce0e: Waiting
dcd34d50d5ee: Waiting
a3ed95caeb02: Verifying Checksum
a3ed95caeb02: Download complete
cd23f57692f8: Verifying Checksum
cd23f57692f8: Download complete
dcd34d50d5ee: Verifying Checksum
dcd34d50d5ee: Download complete
d3c51dabc842: Download complete
b4241160ce0e: Download complete
7abee76f69c0: Download complete
6d9e6e7d968b: Download complete
6d9e6e7d968b: Pull complete
a3ed95caeb02: Pull complete
cd23f57692f8: Pull complete
412c0feed608: Download complete
412c0feed608: Pull complete
dcd34d50d5ee: Pull complete
d3c51dabc842: Pull complete
b4241160ce0e: Pull complete
7abee76f69c0: Pull complete
Digest: sha256:5d99aa1120524c801bc8c1a7077e8f5ec122ba16b6dda1a5d3826057f67b9bcb
Status: Downloaded newer image for gcr.io/google_containers/echoserver:1.4
```
Once container has been launched, check that it is running with:
```console
$ curl http://localhost:8080
CLIENT VALUES:
client_address=172.17.0.1
command=GET
real path=/
query=nil
request_version=1.1
request_uri=http://localhost:8080/

SERVER VALUES:
server_version=nginx: 1.10.0 - lua: 10001

HEADERS RECEIVED:
accept=*/*
host=localhost:8080
user-agent=curl/7.68.0
BODY:
-no body in request-
```
Or you can point your browser at: http://localhost:8080

Check the configuration of the running container with:
```console
$ docker inspect ex01-echoserver
```

And also take the time to explore the build of the container image with:
```console
$ docker history --no-trunc gcr.io/google_containers/echoserver:1.4
```

It is possible to see that a README.md file was added into the container image as the last step.  We can access this in two ways.  The first is by overriding the launch command and `cat`ing the file (make sure you type exit to exit the shell and running container):
```console
$ docker run -ti --rm gcr.io/google_containers/echoserver:1.4 sh
# cat README.md
# Echoserver

This is a simple server that responds with the http headers it received.
Image versions >= 1.4 removes the redirect introduced in 1.3.
Image versions >= 1.3 redirect requests on :80 with `X-Forwarded-Proto: http` to :443.
Image versions > 1.0 run an nginx server, and implement the echoserver using lua in the nginx config.
Image versions <= 1.0 run a python http server instead of nginx, and don't redirect any requests.
# exit
```

The second way is to access the running `echoserver` and copy the `README.md` file out of it with:
```console
$ docker cp ex01-echoserver:/README.md echoserver-readme.md
$ cat echoserver-readme.md
# Echoserver

This is a simple server that responds with the http headers it received.
Image versions >= 1.4 removes the redirect introduced in 1.3.
Image versions >= 1.3 redirect requests on :80 with `X-Forwarded-Proto: http` to :443.
Image versions > 1.0 run an nginx server, and implement the echoserver using lua in the nginx config.
Image versions <= 1.0 run a python http server instead of nginx, and don't redirect any requests.
```


Now lets clean up.  List the running containers:
```console
$ docker ps -a
CONTAINER ID   IMAGE                                     COMMAND                  CREATED          STATUS          PORTS                                                        NAMES
5964bc7bade5   gcr.io/google_containers/echoserver:1.4   "nginx -g 'daemon of…"   12 minutes ago   Up 12 minutes   80/tcp, 443/tcp, 0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   ex01-echoserver
```

Clean up:
```console
$ docker rm -f ex01-echoserver
ex01-echoserver
$ rm echoserver-readme.md
```

[Home &#x3e;&#x3e;](../README.md)
