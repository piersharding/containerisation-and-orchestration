Containerisation and Orchestration - exercise 03
================================================

[[_TOC_]]

Runtime Behaviour of Containers
===============================

This directory contains a list of examples that explore aspects of the behaviour and interaction with containers at runtime.  Each example is standalone and should be executable from the commandline.  Copy the contents of each into a shell script file and execute to see what happens.  Examples can be found in  the `./ex03` directory.


Passing application configuration
---------------------------------

Application configuration is passed into containers through 3 mechanisms:

* command line options
* environment variables
* configuration files

Starting with command line options, we can override the `ENTRYPOINT` and `CMD` declarations in a container image at runtime and use this to pass instructions to our application:
```console
$ docker run --rm -ti --entrypoint /bin/sh busybox:1.28.3 -c "echo got parameters: \$0 \$1 \$2" -a -b -c
got parameters: -a -b -c
```
The container entrypoint has been switched to `/bin/sh`, and then at the command line we have passed in a oneline shell script that echos the further commandline parameters passed in.

The following illustrates how to inject environment variables into a container with Docker:
```console
$ docker run --rm -ti --env MY_VAR=wahoo! busybox:1.28.3 printenv
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=f248e3da04b2
TERM=xterm
MY_VAR=wahoo!
HOME=/root
```
The variable `MY_VAR` has been injected into the environment and `printenv` displays it.


Mounting volumes
----------------

Volumes are used for passing any kind of data file/s into a container.

Change into the `ex03` directory before running the following. Here, we mount the `README.md` file into the `busybox` container, list it, and then do a word count (`wc`) on it:
```console
$ docker run --rm -ti --volume $(pwd)/README.md:/README.md busybox:1.28.3 sh -c "ls -l /README.md; wc /README.md"
-rw-rw-r--    1 1000     1000          4214 Jun 18 07:41 /README.md
      127       601      4214 /README.md
```

Mounting a directory is almost an identical process. This next example mounts the current working directory on the host (`$(pwd)`), into the `busybox` container at the `/data` directory, and we list the contents:
```console
$ docker run --rm -ti --volume $(pwd):/data busybox:1.28.3 sh -c "ls -ld /data; ls -latr /data"
drwxrwxr-x    2 1000     1000          4096 Jun 22 12:11 /data
total 44
-rwxrwxr-x    1 1000     1000           602 Jun 18 10:51 ex03-test-signals-bad.sh
-rwxrwxr-x    1 1000     1000           620 Jun 18 10:55 ex03-test-signals-good.sh
-rwxrwxr-x    1 1000     1000           617 Jun 18 11:58 ex03-logging.sh
-rwxrwxr-x    1 1000     1000          1695 Jun 22 12:05 ex03-test-privileges.sh
-rwxr-xr-x    1 1000     1000          1042 Jun 22 12:09 ex03-fun-with-containers.sh
drwxrwxr-x    2 1000     1000          4096 Jun 22 12:11 .
-rw-rw-r--    1 1000     1000         12732 Jun 22 12:17 README.md
drwxr-xr-x    1 root     root          4096 Jun 29 11:44 ..
```


Signals
-------

It is very important to ensure that your containers handle signals correctly.  This will make the difference between fast and slow container teardown times.  Container engines (eg: Docker) rely on signals to tell containers when to shutdwon, and this starts with `SIGTERM`, and then finishes with `SIGKILL` after the timeout period - usually 10 seconds.

The following is an example of what happens when `SIGTERM` is not respected - copy the below to a script file and run:

```console
#!/bin/sh

NAME=ex03-signals-bad

# Launch a container that mishandles SIGTERM
echo ""
echo "Launching the container (${NAME}) that mishandles SIGTERM:"
docker run --rm --name ${NAME} -d busybox:1.28.3 \
       sh -c "_term() { echo 'got SIGTERM!'; }; trap _term SIGTERM; COUNTER=0;while [ 1 ]; do let COUNTER++; echo `date` \${COUNTER}; sleep 1; done"
sleep 1

# Now try and stop that container
echo ""
echo "Now trying to stop ${NAME} - watch the logs and see the timer statistics at the end:"
time -p docker stop ${NAME} &

# what the logs and see how long it takes to exit
docker logs -f ${NAME}
```

Notice how the signal is trapped and then the container continues to run and only exits when the `SIGKILL` is applied, which took over 10 (wall clock) seconds:

```console
$ ./test-signals-bad.sh

Launching the container (ex03-signals-bad) that mishandles SIGTERM:
18132c86fa36a0642d00f7fa7ef6057efb92123099137c430c056e6033e2c226

Now trying to stop ex03-signals-bad - watch the logs and see the timer statistics at the end:
Fri 18 Jun 2021 10:51:35 PM NZST 1
Fri 18 Jun 2021 10:51:35 PM NZST 2
got SIGTERM!
Fri 18 Jun 2021 10:51:35 PM NZST 3
Fri 18 Jun 2021 10:51:35 PM NZST 4
Fri 18 Jun 2021 10:51:35 PM NZST 5
Fri 18 Jun 2021 10:51:35 PM NZST 6
Fri 18 Jun 2021 10:51:35 PM NZST 7
Fri 18 Jun 2021 10:51:35 PM NZST 8
Fri 18 Jun 2021 10:51:35 PM NZST 9
Fri 18 Jun 2021 10:51:35 PM NZST 10
Fri 18 Jun 2021 10:51:35 PM NZST 11
Fri 18 Jun 2021 10:51:35 PM NZST 12
$ real 10.42
  user 0.01
  sys 0.02
```

The following is an example of what happens when `SIGTERM` is handle immediately - copy the below to a script file and run:

```console
#!/bin/sh

NAME=ex03-signals-good

# Launch a container that mishandles SIGTERM
echo ""
echo "Launching the container (${NAME}) that respects SIGTERM:"
docker run --rm --name ${NAME} -d busybox:1.28.3 \
       sh -c "_term() { echo 'got SIGTERM - exit NOW!'; exit 0; }; trap _term SIGTERM; COUNTER=0;while [ 1 ]; do let COUNTER++; echo `date` \${COUNTER}; sleep 1; done"
sleep 1

# Now try and stop that container
echo ""
echo "Now trying to stop ${NAME} - watch the logs and see the timer statistics at the end:"
time -p docker stop ${NAME} &

# what the logs and see how long it takes to exit
docker logs -f ${NAME}
```

The script trapped the `SIGTERM` and exited immediately, which took 1.26 (wall clock) seconds.

```console
$ ./test-signals-good.sh

Launching the container (ex03-signals-good) that respects SIGTERM:
4dad05535827defc477b61ed471869c36853ebcfe23e4ec0dea0508d1c72fb95

Now trying to stop ex03-signals-good - watch the logs and see the timer statistics at the end:
Fri 18 Jun 2021 10:55:45 PM NZST 1
Fri 18 Jun 2021 10:55:45 PM NZST 2
got SIGTERM - exit NOW!
$ real 1.21
  user 0.02
  sys 0.00
```

Apart from improving developer experience by speeding up refresh cycles, it is also important for the managing of large scale distributed applications and availability - imagine if you are doing a redeployment of a service with A/B testing and trying to minimise down time between switches - you can only do this as fast as your slowest startup and teardown cycle of your constituent applications and their interdependencies.


Logging
-------

**Note:** for the `timeout` command on `macos` you will need to `brew install coreutils` for the following examples to work.

The only universal guarantee for logging with containers is that logs output to `stdout` and `stderr` are captured and combined by the container engine.  The following script demonstrates this with Docker.  It is also worth noting that the order of output is not guaranteed as the file handles are flushed at different times - this is evidenced by the changing order of the `stdout` and `stderr` messages.
```console
#!/bin/sh

NAME=ex03-logging

# Launch a container that prints to both stdout and stderr
echo ""
echo "Launching the container (${NAME}) which prints to both stdout and stderr"
docker run --rm --name ${NAME} -d busybox:1.28.3 \
       sh -c "COUNTER=0;while [ 1 ]; do let COUNTER++; echo `date` \${COUNTER} ' - to stdout'; >&2 echo `date` \${COUNTER} ' - to stderr'; sleep 1; done"

# tail the logs to show that both stdout and stderr are captured by the container engine
echo "tail the logs for 5 seconds:"
timeout 5s docker logs -f ${NAME}

# clean up
echo "stopping the container  (${NAME})"
docker stop ${NAME}
```

Produces the following:
```console
$ ./logging.sh

Launching the container (ex03-logging) which prints to both stdout and stderr
e4ff074c294a6f2bafe45fcd6f06eb5833871fe791cbd813f65f7a8f3e0b1451
tail the logs for 5 seconds:
Fri 18 Jun 2021 11:58:25 PM NZST 1  - to stdout
Fri 18 Jun 2021 11:58:25 PM NZST 1  - to stderr
Fri 18 Jun 2021 11:58:25 PM NZST 2  - to stderr
Fri 18 Jun 2021 11:58:25 PM NZST 2  - to stdout
Fri 18 Jun 2021 11:58:25 PM NZST 3  - to stdout
Fri 18 Jun 2021 11:58:25 PM NZST 3  - to stderr
Fri 18 Jun 2021 11:58:25 PM NZST 4  - to stderr
Fri 18 Jun 2021 11:58:25 PM NZST 4  - to stdout
Fri 18 Jun 2021 11:58:25 PM NZST 5  - to stdout
Fri 18 Jun 2021 11:58:25 PM NZST 5  - to stderr
Fri 18 Jun 2021 11:58:25 PM NZST 6  - to stdout
Fri 18 Jun 2021 11:58:25 PM NZST 6  - to stderr
stopping the container  (ex03-logging)
ex03-logging
```

Containers and Privileges
-------------------------

By default, Docker runs containers with a wide range of privileges.  These can be individually manipulated to change the capabilities of a container at runtime.  The following examples shows how these privileges can be dropped at runtime to increase security.

Build a container image on the fly by piping the `Dockerfile` to the `docker build` command:
```console
#!/bin/sh
NAME=test-pscap
TAG=0.0.1
echo "Build the container image with the pscap tools"
cat <<EOF | docker build -t ${NAME}:${TAG} -
FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install libcap-ng-utils iputils-ping curl -y && \
    apt-get clean -y
EOF
```

Run the container image and show what the default capabilities are that Docker gives containers:
```console
#!/bin/sh
NAME=test-pscap
TAG=0.0.1
echo "Run the container with pscap and show me the default capabilities:"
# This will show chown, dac_override, fowner, fsetid, kill,
# setgid, setuid, setpcap, net_bind_service, net_raw, sys_chroot,
#  mknod, audit_write, setfcap
echo "pscap -a " | \
docker run --rm -i -a stdin -a stdout -a stderr ${NAME}:${TAG} bash
```

Now, drop all the privileges and see what we have:
```console
#!/bin/sh
NAME=test-pscap
TAG=0.0.1
echo "Now drop all privileges and show what we have?: **NOTHING**"
# This will show we have none
echo "pscap -a " | \
docker run --rm --security-opt=no-new-privileges --cap-drop ALL \
   -i -a stdin -a stdout -a stderr ${NAME}:${TAG} bash
```

Lastly, pipe a series of containers together with different sets of permissions, like UNIX pipes, to show how different tasks can be sandboxed with different levels of authority:
```console
#!/bin/sh
NAME=test-pscap
TAG=0.0.1
echo "Now show that we can chain containers together like Unix pipes, with different security capabilities:"
# The first container has no privileges
# The second container will run as the user who executed docker run
#  including having the same uid, gid, and home directory
cat /etc/hosts | \
docker run --rm --security-opt=no-new-privileges \
   --cap-drop ALL -i -a stdin -a stdout -a stderr \
   ${NAME}:${TAG} grep localhost | \
docker run --rm -i -a stdin -a stdout -a stderr \
   -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro \
   --user=$(id -u) -v ${HOME}:${HOME} -w ${HOME} -e HOME=${HOME} \
   ${NAME}:${TAG} awk '{print $1}'
```

Extra Things
============

Impersonating Users
-------------------

For the curious, you can look at a combined example that will run `mplayer` in a container, but impersonate the currently logged in user to access display and sound devices to [play back an MP3 file](./ex03-fun-with-containers.sh).

**Note:** the example only works on Linux and macos. On Linux it needs access to the `/dev/snd` device file.  On macos, you need to install `pulseaudio`.  `pulseaudio` can be installed using `brew install pulseaudio`, and then run the following in a separate terminal: `pulseaudio --load=module-native-protocol-tcp --exit-idle-time=-1`.  Ensure that you confirm the popup to enable `pulseaudio` to listen on a socket. (details in this blog https://devops.datenkollektiv.de/running-a-docker-soundbox-on-mac.html)


Make your own Container Engine
------------------------------

There is an excellent example here that discusses initramfs for booting kernels - https://landley.net/writing/rootfs-programming.html .  From this, there is a great demonstrator script that shows how to prepare a directory tree and switch a process into its own mount namespace.  save the following to `mkchroot.sh`:
```console
#!/bin/bash

function mkchroot
{
  [ $# -lt 2 ] && return

  dest=$1
  shift
  for i in "$@"
  do
    # Get an absolute path for the file
    [ "${i:0:1}" == "/" ] || i=$(which $i)
    # Skip files that already exist at target.
    [ -f "$dest/$i" ] && continue
    if [ -e "$i" ]
    then
      # Create destination path
      d=`echo "$i" | grep -o '.*/'` &&
      mkdir -p "$dest/$d" &&
      # Copy file
      cat "$i" > "$dest/$i" &&
      chmod +x "$dest/$i"
    else
      echo "Not found: $i"
    fi
    # Recursively copy shared libraries' shared libraries.
    mkchroot "$dest" $(ldd "$i" | egrep -o '/.* ')
  done
}

mkchroot "$@"
```

Run `mkchroot.sh` to prepare a directory tree for switching into:
```console
$ ./mkchroot.sh $(pwd)/new_root /bin/sh /bin/ls
$ find new_root/
new_root/
new_root/lib
new_root/lib/x86_64-linux-gnu
new_root/lib/x86_64-linux-gnu/libpthread.so.0
new_root/lib/x86_64-linux-gnu/libdl.so.2
new_root/lib/x86_64-linux-gnu/libselinux.so.1
new_root/lib/x86_64-linux-gnu/libc.so.6
new_root/usr
new_root/usr/lib
new_root/usr/lib/x86_64-linux-gnu
new_root/usr/lib/x86_64-linux-gnu/libpcre2-8.so.0
new_root/bin
new_root/bin/sh
new_root/bin/ls
new_root/lib64
new_root/lib64/ld-linux-x86-64.so.2
```

Now switch the current shell into the `new_root` and launch `/bin/sh`:
```console
$ sudo chroot $(pwd)/new_root /bin/sh
# ls
bin  lib  lib64  usr
# exit
```

[Home &#x3e;&#x3e;](../README.md)
