#!/bin/sh

NAME=ex03-signals-bad

# Launch a container that mishandles SIGTERM
echo ""
echo "Launching the container (${NAME}) that mishandles SIGTERM:"
docker run --rm --name ${NAME} -d busybox:1.28.3 \
       sh -c "_term() { echo 'got SIGTERM!'; }; trap _term SIGTERM; COUNTER=0;while [ 1 ]; do let COUNTER++; echo `date` \${COUNTER}; sleep 1; done"
sleep 1

# Now try and stop that container
echo ""
echo "Now trying to stop ${NAME} - watch the logs and see the timer statistics at the end:"
time -p docker stop ${NAME} &

# what the logs and see how long it takes to exit
docker logs -f ${NAME}

