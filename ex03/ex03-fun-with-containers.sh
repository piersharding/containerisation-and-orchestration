#!/bin/sh
NAME=ex03-test-user-impersonation
TAG=0.0.1

echo "Remove the running ${NAME} container if it exists"
docker inspect ${NAME} >/dev/null 2>&1 && docker rm -f ${NAME}

echo ""
echo ""
echo "Build the container image with mplayer"
cat <<EOF | docker build -t ${NAME}:${TAG} -
FROM ubuntu:18.04
ENV DEBIAN_FRONTEND noninteractive
RUN \
    apt update && \
    apt install mplayer -y

ENTRYPOINT ["/usr/bin/mplayer"]
EOF

echo ""
echo ""
echo "Run the container with mplayer as the currently logged in user integrating display and sound:"

EXTRA_ARGS="-v /tmp/.X11-unix:/tmp/.X11-unix \
            --device /dev/snd \
            -v /etc/machine-id:/etc/machine-id \
            -v /run/user/$(id -u):/run/user/$(id -u) \
            -v /var/lib/dbus:/var/lib/dbus"
if [[ "$OSTYPE" == "darwin"* ]]; then
    
    EXTRA_ARGS="-e PULSE_SERVER=docker.for.mac.localhost \
    -v ${HOME}/.config/pulse:/home/pulseaudio/.config/pulse"
fi
docker run --rm --name ${NAME} --rm \
   --ipc=host \
  -e HOME=${HOME} \
  -e DISPLAY=unix$DISPLAY \
  -v /etc/passwd:/etc/passwd:ro --user=$(id -u) \
  -v ${HOME}:${HOME} -w ${HOME} \
  ${EXTRA_ARGS} \
  -ti ${NAME}:${TAG} /usr/bin/mplayer https://www.doc.govt.nz/Documents/conservation/native-animals/birds/bird-song/morepork-song.mp3
