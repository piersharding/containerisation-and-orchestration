#!/bin/sh

NAME=ex03-logging

# Launch a container that prints to both stdout and stderr
echo ""
echo "Launching the container (${NAME}) which prints to both stdout and stderr"
docker run --rm --name ${NAME} -d busybox:1.28.3 \
       sh -c "COUNTER=0;while [ 1 ]; do let COUNTER++; echo `date` \${COUNTER} ' - to stdout'; >&2 echo `date` \${COUNTER} ' - to stderr'; sleep 1; done" 

# tail the logs to show that both stdout and stderr are captured by the container engine
echo "tail the logs for 5 seconds:"
timeout 5s docker logs -f ${NAME}

# clean up
echo "stopping the container  (${NAME})"
docker stop ${NAME}

